# LarvalThermo_Mosquitodynamics

This repo contains the code and data necessary to replicate the results of the research paper [Larval thermosensitivity shapes adult population dynamics in Anopheles mosquitoes]. It uses data from the field study collection of *Anopheles gambiae s.l.* during the rainy season in Mali, 2015. It uses the concepts of convergent cross-mapping to detect the environmental drivers of mosquito abundance.

In addition, the biological mechanisms are explored using laboratory experiments on the development of *Anopheles coluzzii* larvae in diverse regimes of temperature and rearing density. This part used Mixed-effects modelling to perform model fitting and simulations.

# Requirements

The following programs and packages are required to run the code

- R version 4.0.2 (2020-06-22)
  - Packages for general analysis
    - tidyverse_2.0.0
    - doSNOW_1.0.20
    - doParallel_1.0.16
    - deSolve_1.28
  - Packages for Cross-mapping
    - rEDM_1.2.3
    - Rcpp_1.0.7
  - Graphics packages
    - egg_0.4.5
    - cowplot_1.1.0
    - ghibli_0.3.2
- Lixoft (R) Monolix 2019R2

# Usage

Run each .Rmd file for obtaining the figures and tests shown in the paper

# Support

If you need support to run, understand the framework, or find a bug, please contact Paola Carrillo-Bustamante (carrillo@mpiib-berlin.mpg.de) or Juan Estupiñán (estupinan@mpiib-berlin.mpg.de)
