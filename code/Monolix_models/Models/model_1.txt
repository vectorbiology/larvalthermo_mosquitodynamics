DESCRIPTION: model of larva development
Variables:
L is the number of larvae
P is the number of pupae

The possible effects of the density in this model is to modify the growth rate

[LONGITUDINAL]
input = {gma, delta, Ton, L0}
L0 ={use = regressor}

EQUATION:
;initial conditions
t0=0
P_0 = 0
L_0 = L0

if t < Ton
     coeff = 0
else
     coeff = 1
end

;;; ODE model
ddt_L = -gma*coeff*L - delta*L
ddt_P = gma*coeff*L


OUTPUT:
output = {P}
