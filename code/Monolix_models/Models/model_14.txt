DESCRIPTION: model of larva development
Variables:
L is the number of larvae
P is the number of pupae

The possible effects of the temperature in this model is to modify the death rate and the pupation time

[LONGITUDINAL]
input = {gma, at, ct, L0, Tem, delta, h, h2}

L0 ={use = regressor}
Tem ={use = regressor}

EQUATION:
;initial conditions
t0=0
P_0 = 0
L_0 = L0

Ton = -at*(Tem-28) + ct

if t < Ton
     coeff = 0
else
     coeff = 1
end

;;functional response
eps=exp(h*L)
theta=exp(-h2*L)

;;; ODE model
ddt_L = -gma*theta*coeff*L - delta*eps*L
ddt_P = gma*theta*coeff*L


OUTPUT:
output = {P}
