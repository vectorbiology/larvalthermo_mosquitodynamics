DESCRIPTION: model of larva development
Variables:
L is the number of larvae
P is the number of pupae

The possible effects of the temperature in this model is to modify the death rate and the pupation time

[LONGITUDINAL]
input = {gma, kt, mt, ct, L0, Tem, ad, bd, cd, h, h2}

L0 ={use = regressor}
Tem ={use = regressor}

EQUATION:
;initial conditions
t0=0
P_0 = 0
L_0 = L0

Ton = mt/(1 + exp(kt*(Tem-28))) + ct

if t < Ton
     coeff = 0
else
     coeff = 1
end

;;functional response
theta=ad*(L*h2)*(Tem-28)^2 - bd*(Tem-28) + cd
eps=exp(-log(2)*h*L)

;;; ODE model
ddt_L = -gma*eps*coeff*L - theta*L
ddt_P = gma*eps*coeff*L


OUTPUT:
output = {P}
