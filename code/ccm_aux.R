#### Functions for coupling data with CCM functions
#### Author: Juan Estupinan, 17.08.2021
#### E-mail: estupinan@mpiib-berlin.mpg.de

laf <- function(x,y,lagf){ 
  # taken from Chang_et.al_2020_GCB_SI_Rscript github
  n <- NROW(x)
  x.t <- x
  y.t <- y
  if(lagf<=0){
    # if lagf<0, y is leading
    x.t <- x.t[(1-lagf):n]
    y.t <- y.t[1:(n+lagf)]
    ni <- 1-lagf
    
    return(data.frame(t = ni:n, x.t, y.t))
  } else if(lagf>0){
    # if lagf>0, x is leading
    x.t <- x.t[1:(n-lagf)]
    y.t <- y.t[(1+lagf):n]
    nf <- (n-lagf)
    
    return(data.frame(t = 1:nf, x.t, y.t))
  }             
}

##### create delayed data #####
delay_data <- function(df, dels){
  df_d <- data.frame(t = 1:length(df$Mq), Mq = df$Mq) 
    for(var in names(dels)){
      tmp <- laf(df$Mq, df[[var]], dels[var])
      names(tmp) <- c("t", "Mq", paste0(var,".D"))
    
      df_d <- right_join(df_d, tmp, by = c("t", "Mq"))
    }
  df_d <- df_d %>% filter(if_all(.fns =  ~ !is.na(.x)))  
}

##### Make surrogate data ####
surrogate_data <- function(df, n, seed, meth = "seasonal", TP = 162){
  set.seed(seed)
  surr_wtemp <- make_surrogate_data(df$WTemp_mean, method = meth,
                                    T_period = TP,
                                    num_surr = n)
  surr_atemp <- make_surrogate_data(df$Temperature_mean, method = meth, 
                                    T_period =  TP, 
                                    num_surr = n)
  surr_dewp <- make_surrogate_data(df$Dew_Point_mean, method = meth, 
                                   T_period =  TP, 
                                   num_surr = n)
  surr_humd <- make_surrogate_data(df$Humidity_mean, method = meth, 
                                   T_period =  TP, 
                                   num_surr = n)
  surr_anop <- make_surrogate_data(df$Anopheles, method = meth, 
                                   T_period =  TP, 
                                   num_surr = n)
  surr <- list()
  for(i in 1:n){
    surr[[i]] <- data.frame(Day = df$Day, AT = surr_atemp[,i], Hu = surr_humd[,i], DP = surr_dewp[,i], WT = surr_wtemp[,i], Mq = surr_anop[,i])
  }
  
  return(surr)
}

##### Make surrogate rain data ####
surrogate_rain_data <- function(df, n, seed, meth = "seasonal", TP = 124){
  set.seed(seed)
  surr_anop <- make_surrogate_data(df$Anopheles[28:152], method = meth, 
                                   T_period =  TP, 
                                   num_surr = n)
  surr_rain1 <- make_surrogate_data(df$Rain[28:152], method = meth,
                                    T_period = TP,
                                    num_surr = n)
  surr_rain2 <- rbind(matrix(nrow = 1, ncol = n),
                      make_surrogate_data(df$Rain_2[29:152], method = meth, 
                                          T_period =  TP-1, 
                                          num_surr = n)
                      )
  surr_rain3 <- rbind(matrix(nrow = 2, ncol = n),
                      make_surrogate_data(df$Rain_3[30:152], method = meth, 
                                    T_period =  TP-2, 
                                    num_surr = n)
                      )
  surr_rain4 <- rbind(matrix(nrow = 3, ncol = n),
                      make_surrogate_data(df$Rain_4[31:152], method = meth, 
                                    T_period =  TP-3, 
                                    num_surr = n)
                      )
  surr_rain5 <- rbind(matrix(nrow = 4, ncol = n),
                      make_surrogate_data(df$Rain_5[32:152], method = meth, 
                                    T_period =  TP-4, 
                                    num_surr = n)
                      )
  surr_rain6 <- rbind(matrix(nrow = 5, ncol = n),
                      make_surrogate_data(df$Rain_6[33:152], method = meth, 
                                    T_period =  TP-5, 
                                    num_surr = n)
                      )
  surr_rain7 <- rbind(matrix(nrow = 6, ncol = n),
                      make_surrogate_data(df$Rain_7[34:152], method = meth, 
                                    T_period =  TP-6, 
                                    num_surr = n)
                      )
  surr <- list()
  for(i in 1:n){
    surr[[i]] <- data.frame(Day = df$Day[28:152], Mq = surr_anop[,i],
                            Rain_1 = surr_rain1[,i], Rain_2 = surr_rain2[,i], Rain_3 = surr_rain3[,i],
                            Rain_4 = surr_rain4[,i], Rain_5 = surr_rain5[,i], Rain_6 = surr_rain6[,i], Rain_7 = surr_rain7[,i])
  }
  
  return(surr)
}

##### Return CCM of all samples ####

